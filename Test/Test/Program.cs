﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            int peopleAlive = 0;
            int yearMorePeopleAlive = 0;
            var people = GetListOfPeople();//refers to list to obtain data on people 

            for (int year = 1900; year < 2000; year++)
            {
                var a = people.Where(s => year >= s.DateOfBirth && year <= s.DateOfDeath).Count();//counts how many people were alive for each year

                if (a > peopleAlive)
                {
                    peopleAlive = a;
                    yearMorePeopleAlive = year;
                }
            }
            //below deisplays and declares the most alive and what year
            Console.WriteLine("There were {0} people alive in {1} ", peopleAlive, yearMorePeopleAlive);
            Console.ReadLine();
        }

        private static List<Person> GetListOfPeople() //list class declares the peoples birth and death
        {
            var people = new List<Person>();
            people.Add(new Person(1901, 1960));
            people.Add(new Person(1946, 1998));
            people.Add(new Person(1927, 1981));
            people.Add(new Person(1990, 2000));
            people.Add(new Person(1915, 1954));
            return people;
        }
    }
    public class Person
    {
        public Person(int _dateOfBirth, int _dateOfDeath)
        {
            DateOfBirth = _dateOfBirth;
            DateOfDeath = _dateOfDeath;
        }

        public Int32 DateOfBirth { get; set; }
        public Int32 DateOfDeath { get; set; }
        public Int32 YearsOfLife { get { return DateOfDeath - DateOfBirth; } }
    }
}
